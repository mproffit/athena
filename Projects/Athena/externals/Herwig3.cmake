#
# File specifying the location of Herwig3 to use.
#

set( HERWIG3_LCGVERSION 7.1.5 )
set( HERWIG3_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/herwig++/${HERWIG3_LCGVERSION}/${LCG_PLATFORM} )
